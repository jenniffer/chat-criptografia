jQuery(document).ready(function($) {


  //////////////////////// Initialization
  var baseRef = new Firebase("https://chatcripto.firebaseio.com");
  var messagesRef = baseRef.child("mensajes");
  var userId;

  //get/set the user from local storage
  if (localStorage.getItem('userId') === null) {
    userId = 'user' + parseInt(Math.random() * 1000, 10) + Date.now();
    localStorage.setItem('userId', userId);
  } else {
    userId = localStorage.getItem('userId');
  }
  $("#sysgenerateduserid").text(userId);

  var chatWindow = $("#chatWindow");
  var messageField = $("#mensaje");
  var messageList = $("#messageList");
  var nameField = $("#name");


  fireHangman.init(baseRef);

  //////////////////////// end Initialization 


  //////////////////////// The key enters Management
  //listener for key enter in the keyboard
  messageField.on('keypress', function(e) {
    if (e.keyCode === 13) {
      var nameTmp;

      if (nameField.val() === '') {
        nameTmp = userId;
      } else {
        nameTmp = nameField.val();
      }

      //create the message
      var message = {
        name: nameTmp,
        message: encodeString(messageField.val(), matrix),
        userId: userId
      };

      //push the message to the firebase model
      messagesRef.push(message);

      //clear the message field
      messageField.val('');
    }
  });

  //////////////////////// End enter key management


  //////////////////////// Management  message reception
  messagesRef.limitToLast(20).on('child_added', function(snapshot) {
    //GET DATA
    var data = snapshot.val();
    var name = data.name || "anonymous";
    var message = data.message;

    //CREATE ELEMENTS MESSAGE & SANITIZE TEXT
    var messageElement = $("<li>");
    var nameElement = $("<label></label>");
    nameElement.text(name);
    messageElement.html(decode(message,matrix)).prepend(nameElement);

    //ADD MESSAGE
    messageList.append(messageElement)

    //SCROLL TO BOTTOM OF MESSAGE LIST
    chatWindow[0].scrollTop = chatWindow[0].scrollHeight;
  });
});

//////////////////////// end management message reception


  var letters = "abcdefghijklmnopqrstuvwxyz";
  //KEY 
  var matrix = [[ 11  ,8],[ 3, 7]];

  ///Method encode couple of letters
  function encodeString(message, matrix) {
    var encodedString = "";
    var counter = 0;
    while(counter<message.length){
      encodedString += encode(message.substring(counter,counter+2), matrix);
      counter += 2;
    }
    return encodedString;
  }
  /////end method 

  ////Method  to encode message
  function encode(message, matrix){
    var numberOfA = letters.lastIndexOf(message.charAt(0));
    var numberOfB = letters.lastIndexOf(message.charAt(1));

    var A = (matrix[0][0]*numberOfA + matrix[1][0]*numberOfB)%letters.length;
    var B = (matrix[0][1]*numberOfA + matrix[1][1]*numberOfB)%letters.length;
    var array = [ A%letters.length , B%letters.length];
    return letters.charAt(A)+letters.charAt(B);
  }
    /////end method encode message

  ////7Method to decode message 
  function decode(message, matrix){
    var invertedMatrix = invertMatrix(matrix);
    return encodeString(message, invertedMatrix);
  }

  function invertMatrix(matrix) {
    var determinatOperation = (letters.length + 1) / ( (matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0]) % letters.length );
    var A = (determinatOperation* (matrix[1][1]%letters.length)) % letters.length;
    var B = (determinatOperation* (letters.length-matrix[0][1])) % letters.length;
    var C = (determinatOperation* (letters.length-matrix[1][0])) % letters.length;
    var D = (determinatOperation* (matrix[0][0]%letters.length)) % letters.length;
    var invertedMatrix = [
      [A, B],
      [C, D]
    ];
    return invertedMatrix;
  }


//////////////////////// Firebase object for comunication
var fireHangman = (function() {

  var firebaseRef,
    messagesRef,
    userId;

  return {
    init: function(context) {
      // Our endpoint
      firebaseRef = context;

      // Setup some references
      messagesRef = firebaseRef.child("mensajes");

      //gets the user ID
      userId = localStorage.getItem('userId');
    }
  };
  ////////////////////////end object 
})();